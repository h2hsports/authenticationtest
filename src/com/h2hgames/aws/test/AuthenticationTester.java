/**
 * 
 */
package com.h2hgames.aws.test;

import java.nio.ByteBuffer;
import java.util.List;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.lambda.AWSLambdaClient;
import com.amazonaws.services.lambda.model.FunctionConfiguration;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.model.ListFunctionsResult;

/**
 * @author mdaconta
 *
 */
public class AuthenticationTester {

	/**
	 * 
	 */
	public AuthenticationTester() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// mdaconta account
			System.out.println("Creating basic AWS credentials for h2huser...");
			BasicAWSCredentials awsCredentials = new BasicAWSCredentials("AKIAIW26W7LSVQ3ID5JA","D6kBNmn0iRM9xgoCE5TGdHH4IVmLjC9+69X4+UgV");
			System.out.println("Creating a Lambda Client with those credentials ...");
			AWSLambdaClient lambdaClient = new AWSLambdaClient(awsCredentials);
			// getting a list of functions
			System.out.println("Getting a list of functions...");
			ListFunctionsResult results = lambdaClient.listFunctions();
			List<FunctionConfiguration> funcs = results.getFunctions();
			for (FunctionConfiguration fc: funcs) {
				System.out.println("Function: " + fc.getFunctionName() + ", ARN: " + fc.getFunctionArn());
			}
			
			// now invoke the authenticate user function
			InvokeRequest request = new InvokeRequest();
			System.out.println("Invoking AuthenticateUser...");
			request.setRequestCredentials(awsCredentials);
			request.setFunctionName("AuthenticateUser");
			request.setPayload("{\"userName\":\"mdaconta\",\"passwordHash\":\"test\"}");
			InvokeResult result = lambdaClient.invoke(request);
			if (result != null) {
				ByteBuffer buf = result.getPayload();
				String sbuf = new String(buf.array());
				System.out.println("Result is: " + sbuf);
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

}
